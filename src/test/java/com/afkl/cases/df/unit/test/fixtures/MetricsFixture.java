package com.afkl.cases.df.unit.test.fixtures;

import com.afkl.cases.df.entities.Metrics;

public class MetricsFixture {

    public static Metrics withDefaultValues() {
        final Metrics metrics = new Metrics();

        metrics.increaseRequestCount(200);
        metrics.increaseRequestCount(200);

        metrics.increaseRequestCount(404);

        metrics.processResponseTime(3000);
        metrics.processResponseTime(2000);
        metrics.processResponseTime(1000);

        return metrics;
    }
}
