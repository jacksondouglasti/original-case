package com.afkl.cases.df.unit.test.providers;

import com.afkl.cases.df.entities.Airport;
import com.afkl.cases.df.entities.Fare;
import com.afkl.cases.df.providers.TravelApiClient;
import com.afkl.cases.df.providers.TravelApiProvider;
import com.afkl.cases.df.unit.test.fixtures.AirportResponseDTOFixture;
import com.afkl.cases.df.unit.test.fixtures.FindFareResponseDTOFixture;
import com.afkl.cases.df.unit.test.fixtures.SearchAirportResponseDTOFixture;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class TravelApiProviderUnitTest {

    @InjectMocks
    private TravelApiProvider travelApiProvider;

    @Mock
    private TravelApiClient travelApiClient;

    @Test
    public void shouldSearchAirports() {
        when(travelApiClient.searchAirports("Ottawa")).thenReturn(SearchAirportResponseDTOFixture.withTwoAirports());

        final List<Airport> airports = travelApiProvider.searchAirports("Ottawa");

        assertThat(airports, hasSize(2));

        final Airport airport1 = airports.get(0);
        assertEquals("YOW", airport1.getCode());
        assertEquals("Ottawa International", airport1.getName());
        assertEquals("Ottawa - Ottawa International (YOW), Canada", airport1.getDescription());
        assertEquals(Double.valueOf(45.32083), airport1.getCoordinates().getLatitude());
        assertEquals(Double.valueOf(-75.67278), airport1.getCoordinates().getLongitude());

        verify(travelApiClient, times(1)).searchAirports("Ottawa");
    }

    @Test
    public void shouldReturnAirportsWithNullCoordinates() {
        when(travelApiClient.searchAirports("Houston")).thenReturn(SearchAirportResponseDTOFixture.withNullCoordinates());

        final List<Airport> airports = travelApiProvider.searchAirports("Houston");

        assertThat(airports, hasSize(1));

        final Airport airport1 = airports.get(0);
        assertEquals("HOU", airport1.getCode());
        assertEquals("Houston", airport1.getName());
        assertEquals("Houston - Houston (HOU), USA", airport1.getDescription());
        assertNull(airport1.getCoordinates());

        verify(travelApiClient, times(1)).searchAirports("Houston");
    }

    @Test
    public void shouldFindFare() {
        when(travelApiClient.findFare("YOW", "XDS", "USD")).thenReturn(FindFareResponseDTOFixture.withDefaultValues());

        final Fare fare = travelApiProvider.findFare("YOW", "XDS", "USD");

        assertEquals("YOW", fare.getOrigin().getCode());
        assertEquals("XDS", fare.getDestination().getCode());
        assertEquals("USD", fare.getCurrency());
        assertEquals(BigDecimal.valueOf(2521.09), fare.getAmount());

        verify(travelApiClient, times(1)).findFare("YOW", "XDS", "USD");
    }

    @Test
    public void shouldFindAirport() throws ExecutionException, InterruptedException {
        when(travelApiClient.findAirport("YOW")).thenReturn(AirportResponseDTOFixture.withDefaultValues());

        final CompletableFuture<Airport> airport = travelApiProvider.findAirport("YOW");

        final Airport airport1 = airport.get();
        assertEquals("YOW", airport1.getCode());
        assertEquals("Ottawa International", airport1.getName());
        assertEquals("Ottawa - Ottawa International (YOW), Canada", airport1.getDescription());
        assertEquals(Double.valueOf(45.32083), airport1.getCoordinates().getLatitude());
        assertEquals(Double.valueOf(-75.67278), airport1.getCoordinates().getLongitude());

        verify(travelApiClient, times(1)).findAirport("YOW");
    }
}
