package com.afkl.cases.df.unit.test.fixtures;

import com.afkl.cases.df.providers.dto.response.FindFareResponseDTO;

import java.math.BigDecimal;

public class FindFareResponseDTOFixture {

    public static FindFareResponseDTO withDefaultValues() {
        return FindFareResponseDTO.builder()
                .amount(BigDecimal.valueOf(2521.09))
                .currency("USD")
                .origin("YOW")
                .destination("XDS")
                .build();
    }
}
