package com.afkl.cases.df.unit.test.entities;

import com.afkl.cases.df.entities.Metrics;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
public class MetricsUnitTest {

    @Test
    public void shouldReturnAllValuesZero() {
        final Metrics metrics = new Metrics();

        assertEquals(Long.valueOf(0), metrics.getRequestCount());
        assertEquals(Long.valueOf(0), metrics.getCountStatus2xx());
        assertEquals(Long.valueOf(0), metrics.getCountStatus4xx());
        assertEquals(Long.valueOf(0), metrics.getCountStatus5xx());
        assertEquals(Long.valueOf(0), metrics.getMinResponseTime());
        assertEquals(Long.valueOf(0), metrics.getMaxResponseTime());
        assertEquals(Long.valueOf(0), metrics.getAverageResponseTime());
    }

    @Test
    public void shouldReturnCountRequests() {
        final Metrics metrics = new Metrics();

        metrics.increaseRequestCount(200);
        metrics.increaseRequestCount(201);
        metrics.increaseRequestCount(204);

        metrics.increaseRequestCount(400);
        metrics.increaseRequestCount(404);

        metrics.increaseRequestCount(500);

        assertEquals(Long.valueOf(6), metrics.getRequestCount());
        assertEquals(Long.valueOf(3), metrics.getCountStatus2xx());
        assertEquals(Long.valueOf(2), metrics.getCountStatus4xx());
        assertEquals(Long.valueOf(1), metrics.getCountStatus5xx());
    }

    @Test
    public void shouldReturnResponseTime() {
        final Metrics metrics = new Metrics();

        metrics.processResponseTime(4000);
        metrics.processResponseTime(3000);
        metrics.processResponseTime(2000);
        metrics.processResponseTime(1000);

        assertEquals(Long.valueOf(1000), metrics.getMinResponseTime());
        assertEquals(Long.valueOf(4000), metrics.getMaxResponseTime());
        assertEquals(Long.valueOf(2500), metrics.getAverageResponseTime());
    }
}
