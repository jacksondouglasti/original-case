package com.afkl.cases.df.unit.test.fixtures;


import com.afkl.cases.df.providers.dto.response.AirportResponseDTO;
import com.afkl.cases.df.providers.dto.response.CoordinatesResponseDTO;

public class AirportResponseDTOFixture {

    public static AirportResponseDTO withDefaultValues() {
        return AirportResponseDTO.builder()
                .code("YOW")
                .name("Ottawa International")
                .description("Ottawa - Ottawa International (YOW), Canada")
                .coordinates(CoordinatesResponseDTO.builder()
                        .latitude(45.32083)
                        .longitude(-75.67278)
                        .build())
                .build();
    }

}
