package com.afkl.cases.df.unit.test.fixtures;

import com.afkl.cases.df.entities.Airport;
import com.afkl.cases.df.entities.Coordinates;
import com.afkl.cases.df.entities.Fare;

import java.math.BigDecimal;

public class FareFixture {

    public static Fare withDefaultValues() {
        return Fare.builder()
                .amount(BigDecimal.valueOf(2521.09))
                .currency("USD")
                .origin(Airport.builder()
                        .code("YOW")
                        .name("Ottawa International")
                        .description("Ottawa - Ottawa International (YOW), Canada")
                        .coordinates(Coordinates.builder()
                                .latitude(45.32083)
                                .longitude(-75.67278)
                                .build())
                        .build())
                .destination(Airport.builder()
                        .code("XDS")
                        .name("Ottawa Railway Station")
                        .description("Ottawa - Ottawa Railway Station (XDS), Canada")
                        .coordinates(Coordinates.builder()
                                .latitude(45.45)
                                .longitude(-75.13333)
                                .build())
                        .build())
                .build();
    }
}
