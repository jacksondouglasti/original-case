package com.afkl.cases.df.unit.test.services;

import com.afkl.cases.df.entities.Airport;
import com.afkl.cases.df.providers.TravelApiProvider;
import com.afkl.cases.df.services.AirportService;
import com.afkl.cases.df.unit.test.fixtures.AirportFixture;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class AirportServiceUnitTest {

    @InjectMocks
    private AirportService airportService;

    @Mock
    private TravelApiProvider travelApiProvider;

    @Test
    public void shouldSearchAirports() {
        when(travelApiProvider.searchAirports("Ottawa")).thenReturn(AirportFixture.withTwoAirports());

        final List<Airport> airports = airportService.searchAirports("Ottawa");

        assertThat(airports, hasSize(2));

        verify(travelApiProvider, times(1)).searchAirports("Ottawa");
    }
}
