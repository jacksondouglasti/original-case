package com.afkl.cases.df.unit.test.fixtures;

import com.afkl.cases.df.entities.Airport;
import com.afkl.cases.df.entities.Coordinates;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class AirportFixture {

    public static List<Airport> withTwoAirports() {
        return Arrays.asList(
                withOttawaInternationalAirport(),
                withOttawaRailwayAirport()
        );
    }

    public static Airport withOttawaInternationalAirport() {
        return Airport.builder()
                .code("YOW")
                .name("Ottawa International")
                .description("Ottawa - Ottawa International (YOW), Canada")
                .coordinates(Coordinates.builder()
                        .latitude(45.32083)
                        .longitude(-75.67278)
                        .build())
                .build();
    }

    public static Airport withOttawaRailwayAirport() {
        return Airport.builder()
                .code("XDS")
                .name("Ottawa Railway Station")
                .description("Ottawa - Ottawa Railway Station (XDS), Canada")
                .coordinates(Coordinates.builder()
                        .latitude(45.45)
                        .longitude(-75.13333)
                        .build())
                .build();
    }

    public static List<Airport> withNullCoordinates() {
        return Collections.singletonList(
                Airport.builder()
                        .code("HOU")
                        .name("Houston")
                        .description("Houston - Houston (HOU), USA")
                        .build()
        );
    }
}
