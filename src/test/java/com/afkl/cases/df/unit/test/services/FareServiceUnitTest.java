package com.afkl.cases.df.unit.test.services;

import com.afkl.cases.df.entities.Fare;
import com.afkl.cases.df.providers.TravelApiProvider;
import com.afkl.cases.df.services.FareService;
import com.afkl.cases.df.unit.test.fixtures.AirportFixture;
import com.afkl.cases.df.unit.test.fixtures.FareFixture;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.concurrent.CompletableFuture;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class FareServiceUnitTest {

    @InjectMocks
    private FareService fareService;

    @Mock
    private TravelApiProvider travelApiProvider;

    @Test
    public void shouldSearchAirports() {
        when(travelApiProvider.findFare("YOW", "XDS", "USD")).thenReturn(FareFixture.withDefaultValues());
        when(travelApiProvider.findAirport("YOW")).thenReturn(CompletableFuture.completedFuture(AirportFixture.withOttawaInternationalAirport()));
        when(travelApiProvider.findAirport("XDS")).thenReturn(CompletableFuture.completedFuture(AirportFixture.withOttawaRailwayAirport()));

        final Fare fare = fareService.findFare("YOW", "XDS", "USD");

        assertEquals("YOW", fare.getOrigin().getCode());
        assertEquals("XDS", fare.getDestination().getCode());
        assertEquals("USD", fare.getCurrency());
        assertEquals(BigDecimal.valueOf(2521.09), fare.getAmount());

        verify(travelApiProvider, times(1)).findFare("YOW", "XDS", "USD");
        verify(travelApiProvider, times(1)).findAirport("YOW");
        verify(travelApiProvider, times(1)).findAirport("XDS");
    }
}
