package com.afkl.cases.df.unit.test.services;

import com.afkl.cases.df.entities.Metrics;
import com.afkl.cases.df.services.MetricsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
public class MetricsServiceUnitTest {

    @InjectMocks
    private MetricsService metricsService;

    @Test
    public void shouldAddAndReturnMetrics() {
        metricsService.increaseCount(200);
        metricsService.increaseCount(400);

        metricsService.responseTime(2000);
        metricsService.responseTime(100);

        final Metrics metrics = metricsService.getMetrics();

        assertEquals(Long.valueOf(2), metrics.getRequestCount());
        assertEquals(Long.valueOf(1), metrics.getCountStatus2xx());
        assertEquals(Long.valueOf(1), metrics.getCountStatus4xx());
        assertEquals(Long.valueOf(0), metrics.getCountStatus5xx());

        assertEquals(Long.valueOf(100), metrics.getMinResponseTime());
        assertEquals(Long.valueOf(2000), metrics.getMaxResponseTime());
        assertEquals(Long.valueOf(1050), metrics.getAverageResponseTime());
    }
}
