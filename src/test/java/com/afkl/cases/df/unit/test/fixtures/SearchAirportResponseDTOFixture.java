package com.afkl.cases.df.unit.test.fixtures;

import com.afkl.cases.df.providers.dto.response.AirportResponseDTO;
import com.afkl.cases.df.providers.dto.response.CoordinatesResponseDTO;
import com.afkl.cases.df.providers.dto.response.EmbeddedResponseDTO;
import com.afkl.cases.df.providers.dto.response.SearchAirportResponseDTO;

import java.util.Arrays;
import java.util.Collections;

public class SearchAirportResponseDTOFixture {

    public static SearchAirportResponseDTO withTwoAirports() {
        return SearchAirportResponseDTO.builder()
                ._embedded(EmbeddedResponseDTO.builder()
                        .locations(Arrays.asList(
                                AirportResponseDTO.builder()
                                        .code("YOW")
                                        .name("Ottawa International")
                                        .description("Ottawa - Ottawa International (YOW), Canada")
                                        .coordinates(CoordinatesResponseDTO.builder()
                                                .latitude(45.32083)
                                                .longitude(-75.67278)
                                                .build())
                                        .build(),
                                AirportResponseDTO.builder()
                                        .code("XDS")
                                        .name("Ottawa Railway Station")
                                        .description("Ottawa - Ottawa Railway Station (XDS), Canada")
                                        .coordinates(CoordinatesResponseDTO.builder()
                                                .latitude(45.45)
                                                .longitude(-75.13333)
                                                .build())
                                        .build()
                        ))
                        .build())
                .build();
    }

    public static SearchAirportResponseDTO withNullCoordinates() {
        return SearchAirportResponseDTO.builder()
                ._embedded(EmbeddedResponseDTO.builder()
                        .locations(Collections.singletonList(
                                AirportResponseDTO.builder()
                                        .code("HOU")
                                        .name("Houston")
                                        .description("Houston - Houston (HOU), USA")
                                        .build()
                        ))
                        .build())
                .build();
    }
}
