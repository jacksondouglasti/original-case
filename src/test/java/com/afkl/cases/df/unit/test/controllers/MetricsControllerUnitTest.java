package com.afkl.cases.df.unit.test.controllers;

import com.afkl.cases.df.controllers.MetricsController;
import com.afkl.cases.df.exceptions.handler.ControllerExceptionHandler;
import com.afkl.cases.df.services.MetricsService;
import com.afkl.cases.df.unit.test.fixtures.MetricsFixture;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class MetricsControllerUnitTest {

    @InjectMocks
    private MetricsController metricsController;

    @Mock
    private MetricsService metricsService;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(metricsController)
                .setControllerAdvice(ControllerExceptionHandler.class)
                .build();
    }

    @Test
    public void shouldGetMetrics() throws Exception {
        when(metricsService.getMetrics()).thenReturn(MetricsFixture.withDefaultValues());

        mockMvc.perform(get("/api/metrics"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.total.requests").value(3))
                .andExpect(jsonPath("$.total.requests2xx").value(2))
                .andExpect(jsonPath("$.total.requests4xx").value(1))
                .andExpect(jsonPath("$.total.requests5xx").value(0))
                .andExpect(jsonPath("$.responseTime.min").value(1000))
                .andExpect(jsonPath("$.responseTime.max").value(3000))
                .andExpect(jsonPath("$.responseTime.average").value(2000))
                .andReturn();

        verify(metricsService, times(1)).getMetrics();
    }

}
