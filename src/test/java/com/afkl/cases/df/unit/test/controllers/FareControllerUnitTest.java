package com.afkl.cases.df.unit.test.controllers;

import com.afkl.cases.df.controllers.FareController;
import com.afkl.cases.df.exceptions.TravelApiNotFoundException;
import com.afkl.cases.df.exceptions.handler.ControllerExceptionHandler;
import com.afkl.cases.df.services.FareService;
import com.afkl.cases.df.unit.test.fixtures.FareFixture;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class FareControllerUnitTest {

    @InjectMocks
    private FareController fareController;

    @Mock
    private FareService fareService;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(fareController)
                .setControllerAdvice(ControllerExceptionHandler.class)
                .build();
    }

    @Test
    public void shouldGetFare() throws Exception {
        when(fareService.findFare("YOW", "XDS", "USD")).thenReturn(FareFixture.withDefaultValues());

        mockMvc.perform(get("/api/fares/YOW/XDS?currency=USD"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.amount").value(2521.09))
                .andExpect(jsonPath("$.currency").value("USD"))
                .andExpect(jsonPath("$.origin.code").value("YOW"))
                .andExpect(jsonPath("$.origin.name").value("Ottawa International"))
                .andExpect(jsonPath("$.origin.description").value("Ottawa - Ottawa International (YOW), Canada"))
                .andExpect(jsonPath("$.origin.coordinates.latitude").value(45.32083))
                .andExpect(jsonPath("$.origin.coordinates.longitude").value(-75.67278))
                .andExpect(jsonPath("$.destination.code").value("XDS"))
                .andExpect(jsonPath("$.destination.name").value("Ottawa Railway Station"))
                .andExpect(jsonPath("$.destination.description").value("Ottawa - Ottawa Railway Station (XDS), Canada"))
                .andExpect(jsonPath("$.destination.coordinates.latitude").value(45.45))
                .andExpect(jsonPath("$.destination.coordinates.longitude").value(-75.13333))
                .andReturn();

        verify(fareService, times(1)).findFare("YOW", "XDS", "USD");
    }

    @Test
    public void shouldReturnNotFound() throws Exception {
        when(fareService.findFare("INVALID", "INVALID", "INVALID")).thenThrow(new TravelApiNotFoundException());

        mockMvc.perform(get("/api/fares/INVALID/INVALID?currency=INVALID"))
                .andExpect(status().isNotFound())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.path").value("/api/fares/INVALID/INVALID"))
                .andExpect(jsonPath("$.message").value("Not found"))
                .andExpect(jsonPath("$.status").value(404))
                .andExpect(jsonPath("$.time").exists())
                .andReturn();

        verify(fareService, times(1)).findFare("INVALID", "INVALID", "INVALID");
    }

}
