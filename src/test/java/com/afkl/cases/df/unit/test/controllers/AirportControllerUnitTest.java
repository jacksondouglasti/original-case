package com.afkl.cases.df.unit.test.controllers;

import com.afkl.cases.df.controllers.AirportController;
import com.afkl.cases.df.exceptions.TravelApiNotFoundException;
import com.afkl.cases.df.exceptions.handler.ControllerExceptionHandler;
import com.afkl.cases.df.services.AirportService;
import com.afkl.cases.df.unit.test.fixtures.AirportFixture;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class AirportControllerUnitTest {

    @InjectMocks
    private AirportController airportController;

    @Mock
    private AirportService airportService;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(airportController)
                .setControllerAdvice(ControllerExceptionHandler.class)
                .build();
    }

    @Test
    public void shouldGetAirportsWithTerm() throws Exception {
        when(airportService.searchAirports("Ottawa")).thenReturn(AirportFixture.withTwoAirports());

        mockMvc.perform(get("/api/airports?term=Ottawa"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$.[0]code").value("YOW"))
                .andExpect(jsonPath("$.[0]name").value("Ottawa International"))
                .andExpect(jsonPath("$.[0]description").value("Ottawa - Ottawa International (YOW), Canada"))
                .andExpect(jsonPath("$.[0]coordinates.latitude").value(45.32083))
                .andExpect(jsonPath("$.[0]coordinates.longitude").value(-75.67278))
                .andExpect(jsonPath("$.[1]code").value("XDS"))
                .andExpect(jsonPath("$.[1]name").value("Ottawa Railway Station"))
                .andExpect(jsonPath("$.[1]description").value("Ottawa - Ottawa Railway Station (XDS), Canada"))
                .andExpect(jsonPath("$.[1]coordinates.latitude").value(45.45))
                .andExpect(jsonPath("$.[1]coordinates.longitude").value(-75.13333))
                .andReturn();

        verify(airportService, times(1)).searchAirports("Ottawa");
    }

    @Test
    public void shouldGetAirportsWithTermAndNullCoordinates() throws Exception {
        when(airportService.searchAirports("Houston")).thenReturn(AirportFixture.withNullCoordinates());

        mockMvc.perform(get("/api/airports?term=Houston"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$.[0]code").value("HOU"))
                .andExpect(jsonPath("$.[0]name").value("Houston"))
                .andExpect(jsonPath("$.[0]description").value("Houston - Houston (HOU), USA"))
                .andExpect(jsonPath("$.[0]coordinates").doesNotExist())
                .andReturn();

        verify(airportService, times(1)).searchAirports("Houston");
    }

    @Test
    public void shouldGetAirportsWithoutTerm() throws Exception {
        when(airportService.searchAirports(null)).thenReturn(AirportFixture.withTwoAirports());

        mockMvc.perform(get("/api/airports"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$", hasSize(2)))
                .andReturn();

        verify(airportService, times(1)).searchAirports(null);
    }

    @Test
    public void shouldReturnNotFound() throws Exception {
        when(airportService.searchAirports("InvalidName")).thenThrow(new TravelApiNotFoundException());

        mockMvc.perform(get("/api/airports?term=InvalidName"))
                .andExpect(status().isNotFound())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.path").value("/api/airports"))
                .andExpect(jsonPath("$.message").value("Not found"))
                .andExpect(jsonPath("$.status").value(404))
                .andExpect(jsonPath("$.time").exists())
                .andReturn();

        verify(airportService, times(1)).searchAirports("InvalidName");
    }

}
