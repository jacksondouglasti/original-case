package com.afkl.cases.df.configuration;

import com.afkl.cases.df.services.MetricsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;

@Component
public class MetricFilter implements Filter {

    @Autowired
    private MetricsService metricsService;

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws java.io.IOException, ServletException {
        chain.doFilter(request, response);

        final int status = ((HttpServletResponse) response).getStatus();
        metricsService.increaseCount(status);
    }
}
