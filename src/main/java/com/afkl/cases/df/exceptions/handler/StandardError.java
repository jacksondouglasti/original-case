package com.afkl.cases.df.exceptions.handler;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;

public class StandardError {

    private String path;
    private String message;
    private int status;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss.SSS")
    private LocalDateTime time;

    public StandardError(final String path, final String message, final int status, final LocalDateTime time) {
        this.path = path;
        this.message = message;
        this.status = status;
        this.time = time;
    }

    public String getPath() {
        return path;
    }

    public String getMessage() {
        return message;
    }

    public int getStatus() {
        return status;
    }

    public LocalDateTime getTime() {
        return time;
    }
}
