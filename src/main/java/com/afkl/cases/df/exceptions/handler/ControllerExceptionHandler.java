package com.afkl.cases.df.exceptions.handler;

import com.afkl.cases.df.exceptions.TravelApiNotFoundException;
import com.afkl.cases.df.exceptions.TravelApiResponseException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@ControllerAdvice
public class ControllerExceptionHandler {

    /**
     * Invoked when occur any generic exception.
     *
     * @param e
     * @param request
     * @return response with HttpStatus INTERNAL_SERVER_ERROR
     */
    @ExceptionHandler(Throwable.class)
    public ResponseEntity<StandardError> exception(final Throwable e, final HttpServletRequest request) {
        final StandardError error = new StandardError(request.getRequestURI(), "Server error", HttpStatus.INTERNAL_SERVER_ERROR.value(), LocalDateTime.now());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
    }

    /**
     * Invoked when occur a TravelApiNotFoundException.
     *
     * @param e
     * @param request
     * @return response with HttpStatus NOT_FOUND
     */
    @ExceptionHandler(TravelApiNotFoundException.class)
    public ResponseEntity<StandardError> travelApiNotFoundException(final TravelApiNotFoundException e, final HttpServletRequest request) {
        final StandardError error = new StandardError(request.getRequestURI(), "Not found", HttpStatus.NOT_FOUND.value(), LocalDateTime.now());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
    }

    /**
     * Invoked when occur a TravelApiResponseException.
     *
     * @param e
     * @param request
     * @return response with HttpStatus INTERNAL_SERVER_ERROR
     */
    @ExceptionHandler(TravelApiResponseException.class)
    public ResponseEntity<StandardError> travelApiResponseException(final TravelApiResponseException e, final HttpServletRequest request) {
        final StandardError error = new StandardError(request.getRequestURI(), "Server error", HttpStatus.INTERNAL_SERVER_ERROR.value(), LocalDateTime.now());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
    }
}
