package com.afkl.cases.df.exceptions;

public class ParallelProcessException extends RuntimeException {

    public ParallelProcessException(final String message) {
        super(message);
    }
}
