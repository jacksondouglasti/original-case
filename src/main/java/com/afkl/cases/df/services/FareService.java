package com.afkl.cases.df.services;

import com.afkl.cases.df.entities.Airport;
import com.afkl.cases.df.entities.Fare;
import com.afkl.cases.df.exceptions.ParallelProcessException;
import com.afkl.cases.df.providers.TravelApiProvider;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Slf4j
@Service
@AllArgsConstructor
public class FareService {

    private final TravelApiProvider travelApiProvider;

    public Fare findFare(final String originCode, final String destinationCode, final String currency) {
        final Fare fare = travelApiProvider.findFare(originCode, destinationCode, currency);

        final CompletableFuture<Airport> airportOrigin = travelApiProvider.findAirport(fare.getOrigin().getCode());
        final CompletableFuture<Airport> airportDestination = travelApiProvider.findAirport(fare.getDestination().getCode());

        CompletableFuture.allOf(airportOrigin, airportDestination).join();

        try {
            return fare.toBuilder()
                    .origin(airportOrigin.get())
                    .destination(airportDestination.get())
                    .build();
        } catch (InterruptedException | ExecutionException e) {
            log.error("Error when trying to get Airport from CompletableFeature", e);
            throw new ParallelProcessException(e.getMessage());
        }
    }
}
