package com.afkl.cases.df.services;

import com.afkl.cases.df.entities.Airport;
import com.afkl.cases.df.providers.TravelApiProvider;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
public class AirportService {

    private final TravelApiProvider travelApiProvider;

    public List<Airport> searchAirports(final String term) {
        final List<Airport> airports = travelApiProvider.searchAirports(term);

        log.info("Returning search airports term={} listSize={}", term, airports.size());

        return airports;
    }

}
