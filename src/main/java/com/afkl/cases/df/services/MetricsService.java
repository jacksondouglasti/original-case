package com.afkl.cases.df.services;

import com.afkl.cases.df.entities.Metrics;
import org.springframework.stereotype.Service;

@Service
public class MetricsService {

    private Metrics metrics;

    public MetricsService() {
        metrics = new Metrics();
    }

    public void increaseCount(final int httpStatus) {
        metrics.increaseRequestCount(httpStatus);
    }

    public void responseTime(final long timeElapsed) {
        metrics.processResponseTime(timeElapsed);
    }

    public Metrics getMetrics() {
        return metrics;
    }
}
