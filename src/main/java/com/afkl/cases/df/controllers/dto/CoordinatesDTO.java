package com.afkl.cases.df.controllers.dto;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class CoordinatesDTO {

    private Double latitude;
    private Double longitude;
}
