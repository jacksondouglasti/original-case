package com.afkl.cases.df.controllers;

import com.afkl.cases.df.controllers.converters.MetricsConverter;
import com.afkl.cases.df.controllers.dto.MetricsDTO;
import com.afkl.cases.df.services.MetricsService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping(value = "/api/metrics")
public class MetricsController {

    private final MetricsService metricsService;

    @GetMapping
    public ResponseEntity<MetricsDTO> getMetrics() {
        return ResponseEntity.ok(MetricsConverter.toDTO(metricsService.getMetrics()));
    }
}

