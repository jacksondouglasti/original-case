package com.afkl.cases.df.controllers.dto;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class TotalRequestsDTO {

    private Long requests;
    private Long requests2xx;
    private Long requests4xx;
    private Long requests5xx;

}
