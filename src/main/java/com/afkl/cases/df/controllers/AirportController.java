package com.afkl.cases.df.controllers;

import com.afkl.cases.df.controllers.converters.AirportConverter;
import com.afkl.cases.df.controllers.dto.AirportDTO;
import com.afkl.cases.df.entities.Airport;
import com.afkl.cases.df.services.AirportService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/api/airports")
@AllArgsConstructor
public class AirportController {

    private final AirportService airportService;

    @GetMapping
    public ResponseEntity<List<AirportDTO>> searchAirports(@RequestParam(value = "term", required = false) final String term) {
        final List<Airport> airports = airportService.searchAirports(term);
        return ResponseEntity.ok(AirportConverter.toDTOList(airports));
    }

}
