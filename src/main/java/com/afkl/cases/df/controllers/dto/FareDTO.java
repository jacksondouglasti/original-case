package com.afkl.cases.df.controllers.dto;

import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;

@Value
@Builder
public class FareDTO {

    private BigDecimal amount;
    private String currency;
    private AirportDTO origin;
    private AirportDTO destination;

}
