package com.afkl.cases.df.controllers.dto;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class MetricsDTO {

    private TotalRequestsDTO total;
    private ResponseTimeDTO responseTime;

}
