package com.afkl.cases.df.controllers;

import com.afkl.cases.df.controllers.converters.FareConverter;
import com.afkl.cases.df.controllers.dto.FareDTO;
import com.afkl.cases.df.entities.Fare;
import com.afkl.cases.df.services.FareService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(value = "/api/fares")
@AllArgsConstructor
public class FareController {

    private final FareService fareService;

    @GetMapping("/{originCode}/{destinationCode}")
    public ResponseEntity<FareDTO> findFare(@PathVariable final String originCode,
                                            @PathVariable final String destinationCode,
                                            @RequestParam final String currency) {
        final Fare fare = fareService.findFare(originCode, destinationCode, currency);
        return ResponseEntity.ok(FareConverter.toDTO(fare));
    }

}
