package com.afkl.cases.df.controllers.dto;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class AirportDTO {

    private String code;
    private String name;
    private String description;
    private CoordinatesDTO coordinates;
}
