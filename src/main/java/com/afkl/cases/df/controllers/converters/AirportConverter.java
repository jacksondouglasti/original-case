package com.afkl.cases.df.controllers.converters;

import com.afkl.cases.df.controllers.dto.AirportDTO;
import com.afkl.cases.df.controllers.dto.CoordinatesDTO;
import com.afkl.cases.df.entities.Airport;
import com.afkl.cases.df.entities.Coordinates;
import org.springframework.util.Assert;

import java.util.List;
import java.util.stream.Collectors;

public class AirportConverter {

    public static List<AirportDTO> toDTOList(final List<Airport> airports) {
        Assert.notNull(airports, "Airports cannot be null");

        return airports.stream().map(AirportConverter::toDTO).collect(Collectors.toList());
    }

    public static AirportDTO toDTO(final Airport airport) {
        Assert.notNull(airport, "Airports cannot be null");

        return AirportDTO.builder()
                .code(airport.getCode())
                .name(airport.getName())
                .description(airport.getDescription())
                .coordinates(toCoordinatesDTO(airport.getCoordinates()))
                .build();
    }

    private static CoordinatesDTO toCoordinatesDTO(final Coordinates coordinates) {
        if (coordinates == null) {
            return null;
        }

        return CoordinatesDTO.builder()
                .latitude(coordinates.getLatitude())
                .longitude(coordinates.getLongitude())
                .build();
    }
}
