package com.afkl.cases.df.controllers.converters;

import com.afkl.cases.df.controllers.dto.FareDTO;
import com.afkl.cases.df.entities.Fare;
import org.springframework.util.Assert;

public class FareConverter {

    public static FareDTO toDTO(final Fare fare) {
        Assert.notNull(fare, "Fare cannot be null");

        return FareDTO.builder()
                .amount(fare.getAmount())
                .currency(fare.getCurrency())
                .origin(AirportConverter.toDTO(fare.getOrigin()))
                .destination(AirportConverter.toDTO(fare.getDestination()))
                .build();
    }
}
