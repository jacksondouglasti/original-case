package com.afkl.cases.df.controllers.dto;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class ResponseTimeDTO {

    private Long min;
    private Long max;
    private Long average;

}
