package com.afkl.cases.df.controllers.converters;

import com.afkl.cases.df.controllers.dto.MetricsDTO;
import com.afkl.cases.df.controllers.dto.ResponseTimeDTO;
import com.afkl.cases.df.controllers.dto.TotalRequestsDTO;
import com.afkl.cases.df.entities.Metrics;
import org.springframework.util.Assert;

public class MetricsConverter {

    public static MetricsDTO toDTO(final Metrics metrics) {
        Assert.notNull(metrics, "Metrics cannot be null");

        return MetricsDTO.builder()
                .total(TotalRequestsDTO.builder()
                        .requests(metrics.getRequestCount())
                        .requests2xx(metrics.getCountStatus2xx())
                        .requests4xx(metrics.getCountStatus4xx())
                        .requests5xx(metrics.getCountStatus5xx())
                        .build())
                .responseTime(ResponseTimeDTO.builder()
                        .min(metrics.getMinResponseTime())
                        .max(metrics.getMaxResponseTime())
                        .average(metrics.getAverageResponseTime())
                        .build())
                .build();
    }
}
