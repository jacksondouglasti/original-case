package com.afkl.cases.df.providers.converters;

import com.afkl.cases.df.entities.Airport;
import com.afkl.cases.df.entities.Coordinates;
import com.afkl.cases.df.providers.dto.response.AirportResponseDTO;
import com.afkl.cases.df.providers.dto.response.CoordinatesResponseDTO;
import org.springframework.util.Assert;

public class AirportResponseConverter {

    public static Airport toEntity(final AirportResponseDTO airportDTO) {
        Assert.notNull(airportDTO, "AirportDTO cannot be null");

        return Airport.builder()
                .code(airportDTO.getCode())
                .name(airportDTO.getName())
                .description(airportDTO.getDescription())
                .coordinates(toCoordinates(airportDTO.getCoordinates()))
                .build();
    }

    private static Coordinates toCoordinates(final CoordinatesResponseDTO coordinatesDTO) {
        if (coordinatesDTO == null) {
            return null;
        }

        return Coordinates.builder()
                .latitude(coordinatesDTO.getLatitude())
                .longitude(coordinatesDTO.getLongitude())
                .build();
    }
}
