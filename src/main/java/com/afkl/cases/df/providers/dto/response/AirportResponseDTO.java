package com.afkl.cases.df.providers.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AirportResponseDTO {

    private String code;
    private String name;
    private String description;
    private CoordinatesResponseDTO coordinates;
}
