package com.afkl.cases.df.providers;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.http.AccessTokenRequiredException;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.resource.UserRedirectRequiredException;
import org.springframework.security.oauth2.client.token.AccessTokenProvider;
import org.springframework.security.oauth2.client.token.AccessTokenProviderChain;
import org.springframework.security.oauth2.client.token.AccessTokenRequest;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsAccessTokenProvider;
import org.springframework.security.oauth2.common.OAuth2AccessToken;

import java.util.Collections;

public class TravelApiOAuth2FeignRequestInterceptor implements RequestInterceptor {

    private static final String AUTHORIZATION = "Authorization";
    private static final String TOKEN_TYPE = "Bearer ";

    private final OAuth2ProtectedResourceDetails resource;
    private final OAuth2ClientContext oAuth2ClientContext;
    private AccessTokenProvider accessTokenProvider;

    public TravelApiOAuth2FeignRequestInterceptor(final DefaultOAuth2ClientContext defaultOAuth2ClientContext, final OAuth2ProtectedResourceDetails oAuth2ProtectedResourceDetails) {
        resource = oAuth2ProtectedResourceDetails;
        oAuth2ClientContext = defaultOAuth2ClientContext;
        accessTokenProvider = new AccessTokenProviderChain(Collections.singletonList(new ClientCredentialsAccessTokenProvider()));
    }

    @Override
    public void apply(final RequestTemplate template) {
        template.header(AUTHORIZATION, extract());
    }

    private String extract() {
        final OAuth2AccessToken accessToken = getToken();
        return TOKEN_TYPE + accessToken.getValue();
    }

    private OAuth2AccessToken getToken() {
        final OAuth2AccessToken accessToken = oAuth2ClientContext.getAccessToken();
        if (accessToken == null || accessToken.isExpired()) {
            try {
                return acquireAccessToken();
            } catch (final UserRedirectRequiredException e) {
                oAuth2ClientContext.setAccessToken(null);
                final String stateKey = e.getStateKey();
                if (stateKey != null) {
                    Object stateToPreserve = e.getStateToPreserve();
                    if (stateToPreserve == null) {
                        stateToPreserve = "NONE";
                    }
                    oAuth2ClientContext.setPreservedState(stateKey, stateToPreserve);
                }
                throw e;
            }
        }
        return accessToken;
    }

    private OAuth2AccessToken acquireAccessToken() {
        final AccessTokenRequest tokenRequest = oAuth2ClientContext.getAccessTokenRequest();
        if (tokenRequest == null) {
            throw new AccessTokenRequiredException(
                    "Cannot find valid context on request for resource '" + resource.getId() + "'.", resource);
        }
        final String stateKey = tokenRequest.getStateKey();
        if (stateKey != null) {
            tokenRequest.setPreservedState(
                    oAuth2ClientContext.removePreservedState(stateKey));
        }
        final OAuth2AccessToken existingToken = oAuth2ClientContext.getAccessToken();
        if (existingToken != null) {
            oAuth2ClientContext.setAccessToken(existingToken);
        }
        final OAuth2AccessToken obtainableAccessToken;
        obtainableAccessToken = accessTokenProvider.obtainAccessToken(resource,
                tokenRequest);
        if (obtainableAccessToken == null || obtainableAccessToken.getValue() == null) {
            throw new IllegalStateException(
                    "Access token provider returned a null token, which is illegal according to the contract.");
        }
        oAuth2ClientContext.setAccessToken(obtainableAccessToken);
        return obtainableAccessToken;
    }

}
