package com.afkl.cases.df.providers.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FindFareResponseDTO {

    private BigDecimal amount;
    private String currency;
    private String origin;
    private String destination;
}
