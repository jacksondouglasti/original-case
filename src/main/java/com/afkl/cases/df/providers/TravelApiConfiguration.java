package com.afkl.cases.df.providers;

import feign.RequestInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.resource.BaseOAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;

public class TravelApiConfiguration {

    @Value("${security.oauth2.travel-api.tokenUri}")
    protected String tokenUri;

    @Value("${security.oauth2.travel-api.clientId}")
    protected String clientId;

    @Value("${security.oauth2.travel-api.secret}")
    protected String secret;

    @Bean
    public RequestInterceptor oauth2FeignRequestInterceptor() {
        return new TravelApiOAuth2FeignRequestInterceptor(
                new DefaultOAuth2ClientContext(),
                oAuth2ProtectedResourceDetails()
        );
    }

    @Bean
    public OAuth2ProtectedResourceDetails oAuth2ProtectedResourceDetails() {
        final BaseOAuth2ProtectedResourceDetails resourceDetails = new ClientCredentialsResourceDetails();
        resourceDetails.setAccessTokenUri(tokenUri);
        resourceDetails.setClientId(clientId);
        resourceDetails.setClientSecret(secret);
        return resourceDetails;
    }

    @Bean
    public TravelApiErrorDecoder travelApiErrorDecoder() {
        return new TravelApiErrorDecoder();
    }
}
