package com.afkl.cases.df.providers;

import com.afkl.cases.df.providers.dto.response.AirportResponseDTO;
import com.afkl.cases.df.providers.dto.response.FindFareResponseDTO;
import com.afkl.cases.df.providers.dto.response.SearchAirportResponseDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "${travel-api.name}", url = "${travel-api.url}", configuration = TravelApiConfiguration.class)
public interface TravelApiClient {

    @GetMapping(value = "/airports")
    SearchAirportResponseDTO searchAirports(@RequestParam("term") final String term);

    @GetMapping(value = "/airports/{code}")
    AirportResponseDTO findAirport(@RequestParam("code") final String code);

    @GetMapping(value = "/fares/{originCode}/{destinationCode}")
    FindFareResponseDTO findFare(@PathVariable("originCode") final String originCode, @PathVariable("destinationCode") final String destinationCode, @RequestParam("currency") final String currency);
}
