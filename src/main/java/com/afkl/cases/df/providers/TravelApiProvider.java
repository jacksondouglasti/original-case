package com.afkl.cases.df.providers;

import com.afkl.cases.df.entities.Airport;
import com.afkl.cases.df.entities.Fare;
import com.afkl.cases.df.providers.converters.AirportResponseConverter;
import com.afkl.cases.df.providers.converters.FindFareResponseConverter;
import com.afkl.cases.df.providers.converters.SearchAirportResponseConverter;
import com.afkl.cases.df.providers.dto.response.AirportResponseDTO;
import com.afkl.cases.df.providers.dto.response.FindFareResponseDTO;
import com.afkl.cases.df.providers.dto.response.SearchAirportResponseDTO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Slf4j
@Service
@AllArgsConstructor
public class TravelApiProvider {

    private final TravelApiClient travelApiClient;

    public List<Airport> searchAirports(final String term) {
        final SearchAirportResponseDTO searchAirportResponseDTO = travelApiClient.searchAirports(term);
        return SearchAirportResponseConverter.toEntityList(searchAirportResponseDTO);
    }

    public Fare findFare(final String originCode, final String destinationCode, final String currency) {
        final FindFareResponseDTO fare = travelApiClient.findFare(originCode, destinationCode, currency);
        return FindFareResponseConverter.toEntity(fare);
    }

    @Async
    public CompletableFuture<Airport> findAirport(final String code) {
        log.info("Finding airport using code={}", code);

        final AirportResponseDTO airport = travelApiClient.findAirport(code);

        log.info("Returning airport with code={}", code);
        return CompletableFuture.completedFuture(AirportResponseConverter.toEntity(airport));
    }

}
