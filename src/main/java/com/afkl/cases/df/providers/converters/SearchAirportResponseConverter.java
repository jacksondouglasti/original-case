package com.afkl.cases.df.providers.converters;

import com.afkl.cases.df.entities.Airport;
import com.afkl.cases.df.providers.dto.response.SearchAirportResponseDTO;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class SearchAirportResponseConverter {

    public static List<Airport> toEntityList(final SearchAirportResponseDTO response) {
        if (response == null || response.get_embedded() == null || response.get_embedded().getLocations() == null) {
            return Collections.emptyList();
        }

        return response.get_embedded().getLocations().stream().map(AirportResponseConverter::toEntity).collect(Collectors.toList());
    }


}
