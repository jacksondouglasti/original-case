package com.afkl.cases.df.providers.converters;

import com.afkl.cases.df.entities.Airport;
import com.afkl.cases.df.entities.Fare;
import com.afkl.cases.df.providers.dto.response.FindFareResponseDTO;
import org.springframework.util.Assert;

public class FindFareResponseConverter {

    public static Fare toEntity(final FindFareResponseDTO response) {
        Assert.notNull(response, "Airports cannot be null");

        return Fare.builder()
                .amount(response.getAmount())
                .currency(response.getCurrency())
                .origin(Airport.builder()
                        .code(response.getOrigin())
                        .build())
                .destination(Airport.builder()
                        .code(response.getDestination())
                        .build())
                .build();
    }
}
