package com.afkl.cases.df.providers;

import com.afkl.cases.df.exceptions.TravelApiNotFoundException;
import com.afkl.cases.df.exceptions.TravelApiResponseException;
import feign.Response;
import feign.codec.ErrorDecoder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TravelApiErrorDecoder implements ErrorDecoder {

    @Override
    public Exception decode(final String methodKey, final Response response) {
        log.error("Travel API returned an error status={} responseBody={}", response.status(), response.body());

        if (response.status() == 400 || response.status() == 404) {
            return new TravelApiNotFoundException();
        }

        return new TravelApiResponseException();
    }
}
