package com.afkl.cases.df.aop;

import com.afkl.cases.df.services.MetricsService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Aspect
@Component
public class MetricsAspect {

    @Autowired
    private MetricsService metricsService;

    @Around("execution(* com.afkl.cases.df.controllers.*.*(..))")
    public Object process(final ProceedingJoinPoint joinPoint) throws Throwable {
        final long start = System.currentTimeMillis();

        final Object proceed = joinPoint.proceed();

        final long timeElapsed = System.currentTimeMillis() - start;

        metricsService.responseTime(timeElapsed);

        log.info("Request processed in " + timeElapsed + " milliseconds");

        return proceed;
    }

}
