package com.afkl.cases.df.entities;

import com.afkl.cases.df.entities.enums.HttpStatusCode;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;

public class Metrics {

    private AtomicLong requestCount;
    private AtomicLong minResponseTime;
    private AtomicLong maxResponseTime;

    private AtomicLong responseTimeCount;
    private AtomicLong totalResponseTime;

    private ConcurrentMap<HttpStatusCode, Long> statusMetric;

    public Metrics() {
        requestCount = new AtomicLong(0);

        responseTimeCount = new AtomicLong(0);
        totalResponseTime = new AtomicLong(0);

        statusMetric = new ConcurrentHashMap<>();
    }

    /**
     * Increase request count.
     *
     * @param httpStatus to be used to increase
     */
    public void increaseRequestCount(final int httpStatus) {
        statusMetric.merge(HttpStatusCode.fromHttpStatus(httpStatus), 1L, (a, b) -> a + b);

        requestCount.incrementAndGet();
    }

    /**
     * Elapsed time to process.
     *
     * @param timeElapsed to be processed
     */
    public void processResponseTime(final long timeElapsed) {
        if (minResponseTime == null) {
            minResponseTime = new AtomicLong(timeElapsed);
        } else {
            minResponseTime.set(Math.min(minResponseTime.get(), timeElapsed));
        }

        if (maxResponseTime == null) {
            maxResponseTime = new AtomicLong(timeElapsed);
        } else {
            maxResponseTime.set(Math.max(maxResponseTime.get(), timeElapsed));
        }

        totalResponseTime.set(totalResponseTime.get() + timeElapsed);
        responseTimeCount.incrementAndGet();
    }

    public Long getRequestCount() {
        return requestCount.get();
    }

    public Long getMinResponseTime() {
        return minResponseTime != null ? minResponseTime.get() : 0L;
    }

    public Long getMaxResponseTime() {
        return maxResponseTime != null ? maxResponseTime.get() : 0L;
    }

    public Long getAverageResponseTime() {
        return responseTimeCount.get() > 0 ? totalResponseTime.get() / responseTimeCount.get() : 0L;
    }

    public Long getCountStatus2xx() {
        final Long requests2xx = statusMetric.get(HttpStatusCode.HTTP_2xx);
        return requests2xx != null ? requests2xx : 0L;
    }

    public Long getCountStatus4xx() {
        final Long requests4xx = statusMetric.get(HttpStatusCode.HTTP_4xx);
        return requests4xx != null ? requests4xx : 0L;
    }

    public Long getCountStatus5xx() {
        final Long requests5xx = statusMetric.get(HttpStatusCode.HTTP_5xx);
        return requests5xx != null ? requests5xx : 0L;
    }
}
