package com.afkl.cases.df.entities;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Airport {

    private String code;
    private String name;
    private String description;
    private Coordinates coordinates;

}
