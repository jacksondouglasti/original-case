package com.afkl.cases.df.entities;

import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;

@Value
@Builder(toBuilder = true)
public class Fare {

    private BigDecimal amount;
    private String currency;
    private Airport origin;
    private Airport destination;

}
