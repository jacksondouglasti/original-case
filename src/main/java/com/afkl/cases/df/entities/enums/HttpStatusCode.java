package com.afkl.cases.df.entities.enums;

import org.springframework.http.HttpStatus;

public enum HttpStatusCode {

    HTTP_2xx, HTTP_4xx, HTTP_5xx;

    public static HttpStatusCode fromHttpStatus(final int httpStatus) {
        if (HttpStatus.valueOf(httpStatus).is2xxSuccessful()) {
            return HTTP_2xx;
        } else if (HttpStatus.valueOf(httpStatus).is4xxClientError()) {
            return HTTP_4xx;
        } else if (HttpStatus.valueOf(httpStatus).is5xxServerError()) {
            return HTTP_5xx;
        }

        return null;
    }
}
