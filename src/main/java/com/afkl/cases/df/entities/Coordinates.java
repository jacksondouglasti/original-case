package com.afkl.cases.df.entities;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Coordinates {

    private Double latitude;
    private Double longitude;
}
